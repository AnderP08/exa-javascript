// Crear una función JavaScript para registrar los datos de clientes
// (Cédula, apellidos, nombres, dirección, teléfono). Poner el código en la respuesta
// Y enviar el enlace de GitLab al correo institucional patricia.quiroz@uleam.edu.ec *

/*let IngCed = document.getElementById("dni");
let ingApell = document.getElementById("apellido");
let ingNombre = document.getElementById("nombre");
let ingDireccion = document.getElementById("direccion");
let ingTelefono = document.getElementById("numCelular");*/

//variables ejemplos de como seria el ingreso por prompt
/*let IngCed = prompt("Ingresa cedula");
let ingApell = prompt("Ingresa apellido");
let ingNombre = prompt("Ingresa nombre");
let ingDireccion = prompt("Ingresa direccion");
let ingTelefono = prompt("Ingresa telefono");*/

//datos se encuentran el index.html
document.getElementById("boton").addEventListener("click", (e) => {
    e.preventDefault();
    let error = validacion();
    if (error[0]) {
        alert(error[1]);
    } else alert('Formulario enviado');
})

const validacion = () => {
    let error = [];

    if (IngCed.value == "" || !(/^\d{10}$/.test(IngCed.value))) {
        error[0] = true;
        error[1] = '¡Campo vacio, Ingrese número de cédula o verifique que esta correcta!';
        return error;

    } else if (ingApell.value == null || ingApell.value == 0) {
        error[0] = true;
        error[1] = '¡Ingrese apellido!';
        return error;

    } else if (ingNombre.value == null || ingNombre.value == 0) {
        error[0] = true;
        error[1] = '¡Ingrese nombre!';
        return error;

    } else if (ingDireccion.value == null || ingDireccion.value == 0) {
        error[0] = true;
        error[1] = '¡Ingresa dirección!';
        return error;

    } else if (!(/^\d{10}$/.test(ingTelefono.value))) {
        error[0] = true;
        error[1] = '¡Ingresa número telefonico!';
        return error;
    }

    error[0] = false;
    return error;

};
